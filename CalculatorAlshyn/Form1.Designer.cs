﻿namespace CalculatorAlshyn
{
    partial class calculator
    {
        /// <summary>
        /// Требуется переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором форм Windows

        /// <summary>
        /// Обязательный метод для поддержки конструктора - не изменяйте
        /// содержимое данного метода при помощи редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            this.textBox = new System.Windows.Forms.TextBox();
            this.add = new System.Windows.Forms.Button();
            this.subtract = new System.Windows.Forms.Button();
            this.multiply = new System.Windows.Forms.Button();
            this.divide = new System.Windows.Forms.Button();
            this.equal = new System.Windows.Forms.Button();
            this.seven = new System.Windows.Forms.Button();
            this.eight = new System.Windows.Forms.Button();
            this.nine = new System.Windows.Forms.Button();
            this.six = new System.Windows.Forms.Button();
            this.five = new System.Windows.Forms.Button();
            this.four = new System.Windows.Forms.Button();
            this.three = new System.Windows.Forms.Button();
            this.two = new System.Windows.Forms.Button();
            this.one = new System.Windows.Forms.Button();
            this.zero = new System.Windows.Forms.Button();
            this.ac = new System.Windows.Forms.Button();
            this.mc = new System.Windows.Forms.Button();
            this.c = new System.Windows.Forms.Button();
            this.mpluc = new System.Windows.Forms.Button();
            this.dot = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // textBox
            // 
            this.textBox.Location = new System.Drawing.Point(12, 12);
            this.textBox.Name = "textBox";
            this.textBox.Size = new System.Drawing.Size(130, 20);
            this.textBox.TabIndex = 0;
            // 
            // add
            // 
            this.add.Location = new System.Drawing.Point(12, 38);
            this.add.Name = "add";
            this.add.Size = new System.Drawing.Size(25, 25);
            this.add.TabIndex = 1;
            this.add.Text = "+";
            this.add.UseVisualStyleBackColor = true;
            // 
            // subtract
            // 
            this.subtract.Location = new System.Drawing.Point(43, 38);
            this.subtract.Name = "subtract";
            this.subtract.Size = new System.Drawing.Size(25, 25);
            this.subtract.TabIndex = 2;
            this.subtract.Text = "-";
            this.subtract.UseVisualStyleBackColor = true;
            // 
            // multiply
            // 
            this.multiply.Location = new System.Drawing.Point(74, 38);
            this.multiply.Name = "multiply";
            this.multiply.Size = new System.Drawing.Size(25, 25);
            this.multiply.TabIndex = 3;
            this.multiply.Text = "x";
            this.multiply.UseVisualStyleBackColor = true;
            // 
            // divide
            // 
            this.divide.Location = new System.Drawing.Point(105, 38);
            this.divide.Name = "divide";
            this.divide.Size = new System.Drawing.Size(37, 25);
            this.divide.TabIndex = 4;
            this.divide.Text = "/";
            this.divide.UseVisualStyleBackColor = true;
            // 
            // equal
            // 
            this.equal.Location = new System.Drawing.Point(105, 69);
            this.equal.Name = "equal";
            this.equal.Size = new System.Drawing.Size(37, 25);
            this.equal.TabIndex = 5;
            this.equal.Text = "=";
            this.equal.UseVisualStyleBackColor = true;
            // 
            // seven
            // 
            this.seven.Location = new System.Drawing.Point(12, 69);
            this.seven.Name = "seven";
            this.seven.Size = new System.Drawing.Size(25, 25);
            this.seven.TabIndex = 6;
            this.seven.Text = "7";
            this.seven.UseVisualStyleBackColor = true;
            this.seven.Click += new System.EventHandler(this.seven_Click);
            // 
            // eight
            // 
            this.eight.Location = new System.Drawing.Point(43, 69);
            this.eight.Name = "eight";
            this.eight.Size = new System.Drawing.Size(25, 25);
            this.eight.TabIndex = 7;
            this.eight.Text = "8";
            this.eight.UseVisualStyleBackColor = true;
            this.eight.Click += new System.EventHandler(this.eight_Click);
            // 
            // nine
            // 
            this.nine.Location = new System.Drawing.Point(74, 69);
            this.nine.Name = "nine";
            this.nine.Size = new System.Drawing.Size(25, 25);
            this.nine.TabIndex = 8;
            this.nine.Text = "9";
            this.nine.UseVisualStyleBackColor = true;
            this.nine.Click += new System.EventHandler(this.nine_Click);
            // 
            // six
            // 
            this.six.Location = new System.Drawing.Point(74, 100);
            this.six.Name = "six";
            this.six.Size = new System.Drawing.Size(25, 25);
            this.six.TabIndex = 9;
            this.six.Text = "6";
            this.six.UseVisualStyleBackColor = true;
            this.six.Click += new System.EventHandler(this.six_Click);
            // 
            // five
            // 
            this.five.Location = new System.Drawing.Point(43, 100);
            this.five.Name = "five";
            this.five.Size = new System.Drawing.Size(25, 25);
            this.five.TabIndex = 10;
            this.five.Text = "5";
            this.five.UseVisualStyleBackColor = true;
            this.five.Click += new System.EventHandler(this.five_Click);
            // 
            // four
            // 
            this.four.Location = new System.Drawing.Point(12, 100);
            this.four.Name = "four";
            this.four.Size = new System.Drawing.Size(25, 25);
            this.four.TabIndex = 11;
            this.four.Text = "4";
            this.four.UseVisualStyleBackColor = true;
            this.four.Click += new System.EventHandler(this.four_Click);
            // 
            // three
            // 
            this.three.Location = new System.Drawing.Point(74, 131);
            this.three.Name = "three";
            this.three.Size = new System.Drawing.Size(25, 25);
            this.three.TabIndex = 12;
            this.three.Text = "3";
            this.three.UseVisualStyleBackColor = true;
            this.three.Click += new System.EventHandler(this.three_Click);
            // 
            // two
            // 
            this.two.Location = new System.Drawing.Point(43, 131);
            this.two.Name = "two";
            this.two.Size = new System.Drawing.Size(25, 25);
            this.two.TabIndex = 13;
            this.two.Text = "2";
            this.two.UseVisualStyleBackColor = true;
            this.two.Click += new System.EventHandler(this.two_Click);
            // 
            // one
            // 
            this.one.Location = new System.Drawing.Point(12, 131);
            this.one.Name = "one";
            this.one.Size = new System.Drawing.Size(25, 25);
            this.one.TabIndex = 14;
            this.one.Text = "1";
            this.one.UseVisualStyleBackColor = true;
            this.one.Click += new System.EventHandler(this.one_Click);
            // 
            // zero
            // 
            this.zero.Location = new System.Drawing.Point(12, 162);
            this.zero.Name = "zero";
            this.zero.Size = new System.Drawing.Size(25, 25);
            this.zero.TabIndex = 15;
            this.zero.Text = "0";
            this.zero.UseVisualStyleBackColor = true;
            this.zero.Click += new System.EventHandler(this.zero_Click);
            // 
            // ac
            // 
            this.ac.Location = new System.Drawing.Point(105, 162);
            this.ac.Name = "ac";
            this.ac.Size = new System.Drawing.Size(37, 25);
            this.ac.TabIndex = 16;
            this.ac.Text = "AC";
            this.ac.UseVisualStyleBackColor = true;
            // 
            // mc
            // 
            this.mc.Location = new System.Drawing.Point(105, 131);
            this.mc.Name = "mc";
            this.mc.Size = new System.Drawing.Size(37, 25);
            this.mc.TabIndex = 17;
            this.mc.Text = "MC";
            this.mc.UseVisualStyleBackColor = true;
            // 
            // c
            // 
            this.c.Location = new System.Drawing.Point(74, 162);
            this.c.Name = "c";
            this.c.Size = new System.Drawing.Size(25, 25);
            this.c.TabIndex = 18;
            this.c.Text = "C";
            this.c.UseVisualStyleBackColor = true;
            // 
            // mpluc
            // 
            this.mpluc.Location = new System.Drawing.Point(105, 100);
            this.mpluc.Name = "mpluc";
            this.mpluc.Size = new System.Drawing.Size(37, 25);
            this.mpluc.TabIndex = 19;
            this.mpluc.Text = "M+";
            this.mpluc.UseVisualStyleBackColor = true;
            // 
            // dot
            // 
            this.dot.Location = new System.Drawing.Point(43, 162);
            this.dot.Name = "dot";
            this.dot.Size = new System.Drawing.Size(25, 25);
            this.dot.TabIndex = 20;
            this.dot.Text = ".";
            this.dot.UseVisualStyleBackColor = true;
            this.dot.Click += new System.EventHandler(this.dot_Click);
            // 
            // calculator
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(148, 196);
            this.Controls.Add(this.dot);
            this.Controls.Add(this.mpluc);
            this.Controls.Add(this.c);
            this.Controls.Add(this.mc);
            this.Controls.Add(this.ac);
            this.Controls.Add(this.zero);
            this.Controls.Add(this.one);
            this.Controls.Add(this.two);
            this.Controls.Add(this.three);
            this.Controls.Add(this.four);
            this.Controls.Add(this.five);
            this.Controls.Add(this.six);
            this.Controls.Add(this.nine);
            this.Controls.Add(this.eight);
            this.Controls.Add(this.seven);
            this.Controls.Add(this.equal);
            this.Controls.Add(this.divide);
            this.Controls.Add(this.multiply);
            this.Controls.Add(this.subtract);
            this.Controls.Add(this.add);
            this.Controls.Add(this.textBox);
            this.Name = "calculator";
            this.Text = "Form1";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox textBox;
        private System.Windows.Forms.Button add;
        private System.Windows.Forms.Button subtract;
        private System.Windows.Forms.Button multiply;
        private System.Windows.Forms.Button divide;
        private System.Windows.Forms.Button equal;
        private System.Windows.Forms.Button seven;
        private System.Windows.Forms.Button eight;
        private System.Windows.Forms.Button nine;
        private System.Windows.Forms.Button six;
        private System.Windows.Forms.Button five;
        private System.Windows.Forms.Button four;
        private System.Windows.Forms.Button three;
        private System.Windows.Forms.Button two;
        private System.Windows.Forms.Button one;
        private System.Windows.Forms.Button zero;
        private System.Windows.Forms.Button ac;
        private System.Windows.Forms.Button mc;
        private System.Windows.Forms.Button c;
        private System.Windows.Forms.Button mpluc;
        private System.Windows.Forms.Button dot;
    }
}

