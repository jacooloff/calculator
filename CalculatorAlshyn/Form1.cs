﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace CalculatorAlshyn
{
    public partial class calculator : Form
    {
        private string _temporaryBuffer;
        private int _consistentBuffer;

        public calculator()
        {
            InitializeComponent();
        }

        private void AddToBuffer(string number)
        {
            _temporaryBuffer = _temporaryBuffer + number;
        }
        private void zero_Click(object sender, EventArgs e)
        {
            AddToBuffer("0");
        }

        private void one_Click(object sender, EventArgs e)
        {
            AddToBuffer("1");
        }

        private void two_Click(object sender, EventArgs e)
        {
            AddToBuffer("2");
        }

        private void three_Click(object sender, EventArgs e)
        {
            AddToBuffer("3");
        }

        private void four_Click(object sender, EventArgs e)
        {
            AddToBuffer("4");
        }

        private void five_Click(object sender, EventArgs e)
        {
            AddToBuffer("5");
        }

        private void six_Click(object sender, EventArgs e)
        {
            AddToBuffer("6");
        }

        private void seven_Click(object sender, EventArgs e)
        {
            AddToBuffer("7");
        }

        private void eight_Click(object sender, EventArgs e)
        {
            AddToBuffer("8");
        }

        private void nine_Click(object sender, EventArgs e)
        {
            AddToBuffer("9");
        }

        private void dot_Click(object sender, EventArgs e)
        {
            AddToFloatBuffer(".");
        }

    }
}
